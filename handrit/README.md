# Overview

A theme suitable for exporting manuscripts. Aspects of this theme include:

* Double-spaced lines
* Courier font (TeX Gyre Cursor)
* Plain hyperlinks
* Single hash for scene separators
* Author, title, and page number in upper-right corner
* Formatted document word count
* Page margins set to one inch
* Basic typography settings (no smart em-dashes, ellipses, etc.)
* The word "END" is centered after the last paragraph

# Credit

Thanks to William Shunn for reviewing the formatting.

https://www.shunn.net/

# Fonts

The following fonts are bundled with the typesetting container:

* [Courier Prime](https://fonts.keenwrite.com/download/courier-prime.zip)
* [Roboto Mono](http://fonts.keenwrite.com/download/roboto-mono.zip)
* [OpenSansEmoji](http://fonts.keenwrite.com/download/open-sans-emoji.zip)

If you have installed the typesetter manually, then you will need to install
the fonts yourself.

## Bundled

The following fonts are bundled with ConTeXt:

* [TeX Gyre Cursor](https://ctan.org/pkg/tex-gyre-cursor)
* [TeX Gyre Pagella Math](https://ctan.org/pkg/tex-gyre-math-pagella)
* [ALMFixed](https://ctan.org/pkg/almfixed)

There is no need to install bundled fonts.

