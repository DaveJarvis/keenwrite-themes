# Last Will and Testament

<!-- PART 1: Initial matters. -->
::: initial
::: domicile
1. This is the last Will and Testament of me, {{document.author.name.full}}, of {{document.author.address.full}}.
:::

::: revocation
1. I revoke all my prior wills and codicils.
:::

::: definitions
1. In this Will:
   1. "Articles" means all items of personal, domestic, and household use or ornament, and includes automobiles and boats, and accessories to them, that I own when I die;
   1. "decide" or "decides" means, when referring to a decision of any person, a decision made in that person's discretion;
   1. "discretion" means sole and uncontrolled discretion to the extent permitted by law;
   1. "Trustee" means both the executor of my Will and the trustee of my estate and any reference to my Trustee includes all genders and the singular or the plural as the context requires; and
   1. "year" means a period of 365 days (or 366 days for leap years) in the Gregorian calendar, used for reckoning time in ordinary affairs.

1. Headings are inserted for convenience only and do not affect how this Will is interpreted.
:::

::: executor
::: appointment
1.
   1. I appoint my {{executor.primary.relationship.title}} {{executor.primary.name.full}} ("{{executor.primary.name.given}}") of {{executor.primary.address.full}} to be my Trustee.
   1. If {{executor.primary.name.given}} dies, is incapable, or is unwilling to act or continue to act as my Trustee, I appoint {{executor.secondary.name.full}} of {{executor.secondary.address.full}} to be my Trustee.
:::
:::

::: guardian
1. I appoint my {{guardian.relationship.title}} {{guardian.name.full}} ("{{guardian.name.given}}") to be the guardian of my minor children. It is my hope that, in accordance with the provisions of the *Family Law Act* of {{document.author.address.region}}, {{guardian.name.given}} will appoint a guardian in {{guardian.pronoun}} Will, or otherwise, to be the guardian of my minor children.
:::
:::

<!-- PART 2: Disposition of the estate. -->
::: disposition
::: vesting
1. I give my Trustee all my property of every kind and wherever located to administer as I direct in this Will. In administering my estate, my Trustee may convert or retain my estate as set out in paragraph 11 of this Will.
1. I direct my Trustee:
:::

::: debts
   1. to pay out of my estate:
      1. my debts, including income taxes payable up to and including the date of my death;
      1. my funeral and other expenses related to this Will and my death;
      1. any financial charges with respect to any property which, pursuant to this will, is transferred free and clear to a beneficiary or beneficiaries; and
      1. all estate, gift, inheritance, succession, and other death taxes or duties payable in respect of all property passing on my death, including:
         1. insurance proceeds on my life payable as a consequence of my death (but excluding the proceeds of insurance upon my life owned by any corporation or owned by any partnership of which I am a partner);
         1. any annuity, registered retirement savings plan, registered retirement income fund, pension, or superannuation benefits payable to any person as a result of my death;
         1. any gift made by me in my lifetime; and
         1. any benefit arising by survivorship,

           my Trustee may pay these taxes whether they are imposed by the law of this jurisdiction or any other, and my Trustee may prepay or delay payment of any taxes or duties.
:::

::: gifts
   1. to deliver my [Article 1] to my [relationship] [recipient full name];
   1. to add to the residue of my estate any items of the Articles listed by this Will that could not be delivered to their intended recipient(s) due to the recipient(s) being deceased, incapable, or unwilling to receive the Articles; and
   1. to pay for the packing, freight, and insurance costs my Trustee decides to be appropriate for delivering any items of the Articles as required by this Will.
:::

::: residue
   1. to give the residue of my estate to {{executor.primary.name.given}}, if she survives me for 30 days;
   1. if {{executor.primary.name.given}} does not survive me for 30 days, to divide the residue of my estate into as many equal shares as there are of my children who are alive at my death, except if any child of mine has died before me and one or more of his or her children are alive at my death, that deceased child will be considered alive for the purposes of the division, and:
      1. with respect to the share created for any child of mine who is alive at my death, hold that share for that child on the following trusts:
         1. pay so much of its income and capital as my Trustee decides is advisable for the care, maintenance, education, and benefit of that child until he or she dies or reaches 25 years of age;
         1. add any income not paid in any year to the capital of that share;
         1. when that child reaches 21 years of age, give that child one-half of what remains of his or her share;
         1. when that child reaches 25 years of age, give that child what remains of that share;
         1. if that child dies before reaching 25 years of age and leaves one or more children surviving him or her, divide what remains of that share equally among those of his or her children alive at his or her death;
         1. if that child dies before reaching 25 years of age and leaves no child surviving him or her, divide what remains of that child's portion equally among the other portions created under the provisions of paragraph 8(e) of this Will;
      1. with respect to the share created for any child of mine who died before me and left one or more of his or her children alive at my death, divide that share equally among those children of that deceased child.
::: <!-- residue -->
::: <!-- disposition -->

<!-- PART 3: Powers of Trustee. -->
::: powers
::: minors
1. If anyone becomes entitled to any part of my estate, is under 19 years of age, and I have not specified terms in this Will on which my Trustee is to hold that part, I direct my Trustee to hold that part, and:
   1. pay as much of the income and capital as my Trustee decides for that person's benefit until that person reaches 19 years of age;
   1. add any unused income to the capital of that person's part of my estate and then pay the capital to that person when he or she reaches 19 years of age, but if that person dies before reaching 19 years of age,
I direct my Trustee to pay that person's part of my estate to that person's estate; and
   1. regardless of paragraph 9(a), and at any time my Trustee decides, pay some or all of that part of my estate to that person's parent or guardian for that person's benefit.
:::

::: payment
1. When my Trustee makes any payment for the benefit of any person under 19 years of age, my Trustee may make that payment to that person's parent or guardian. When the parent or guardian receives that payment, my Trustee is discharged for that payment and need not inquire about how it is used.
:::

::: conversions
1.  When my Trustee administers my estate:
    1. my Trustee may convert my estate or any part of my estate into money or any other form of property or security, and decide how, when, and on what terms;
    1. my Trustee may keep my estate, or any part of it, in the form it is in at my death and for as long as my Trustee decides, even for the duration of the trusts in this Will. This power applies even if:
       1. the property is not an investment authorized under this Will;
       1. a debt is owing on the property; or
       1. the property does not produce income;
    1. my Trustee may invest my estate or any part of my estate in any form of property or security in which a prudent investor might invest; and
    1. if at any time my Trustee is a corporate trustee, the Trustee may invest in that corporation's common trust funds and the corporate trustee need not account for any profit that accrues to it as a result of that investment.
:::

::: allocations
1. When my Trustee divides or distributes my estate, my Trustee may decide which assets of my estate to allocate to any share or interest in my estate (and not necessarily equally among those shares or interests) and the value of each of those assets. Whatever value my Trustee places on those assets will be final and binding on everyone interested in my estate.
:::

::: taxes
1.
   1. My Trustee may make any allocations, elections, and distributions as my Trustee decides are in the best interests of my estate as a whole, including any allocations and elections under the *Income Tax Act of Canada*.
   1. Any election that allocates any portion of the income of my estate to any person for the purposes of the *Income Tax Act* shall not be regarded as a decision by my Trustee to allocate, in fact, that income to that person nor will that decision give that person any right to that income.
   1. As a result of any of those allocations, elections, or distributions made in good faith, my Trustee will not be considered to have breached any duty to maintain an even hand among the beneficiaries and my Trustee will not be liable for any loss to my estate or any beneficiary of my estate.
:::

::: investments
1. When holding, keeping, or investing, my Trustee may invest in any investments authorized by the *Trustee Act* of {{document.author.address.region}} for the investment of trust funds.
:::

::: business
1. If my estate holds any interest in any business, incorporated or otherwise, my Trustee may deal with that interest and may exercise any rights, powers, and privileges in connection with that interest to the same extent as I could if I were alive and the only owner of that interest.
:::

::: property
1. My Trustee, with respect to any property (personal or real) forming part of my estate, may exercise any rights, powers, and privileges in connection with that property to the same extent as I could if I were alive and the only owner of that property. Those powers include:
   1. managing any of that property;
   1. spending money out of the capital and income, or either, to repair, convert, improve, or rebuild any of that property;
   1. razing any building on that property;
   1. erecting any building on that property;
   1. insuring any of that property against whatever risks and in whatever amounts my Trustee decides;
   1. renting, leasing, or permitting the use or occupancy of any of that property for any period of time and on whatever terms that my Trustee decides;
   1. terminating or accepting the surrender of any lease or rental arrangement concerning that property;
   1. granting any option or right of first refusal to any person to purchase or lease that property for any period of time and on whatever terms my Trustee decides;
   1. paying off or renewing any mortgages on that property;
   1. paying any person in any profession, trade, or business to transact any business or trade, or to do any act in relation to that property; and
   1. transferring that property in exchange for other property on whatever terms and conditions my Trustee decides.
:::

::: receipts
1. When my Trustee makes any payment to any organization, society, foundation, association, or corporation, my Trustee may accept the receipt of any person purporting to be the secretary or treasurer or other officer or officers, as the case may be, of that beneficiary. The receipt will discharge my Trustee for that payment and my Trustee need not enquire about how that payment is used.
:::
::: <!-- powers -->

<!-- PART 4: Disinheritance. -->
::: elimination
1. I am choosing to leave no assets to my [relationship] [name of disinherited]. My [relationship] is [Estrangement or Broken Relationship], [Financial Irresponsibility or Misconduct], [Different Philosophies or Values], [Criminal Behaviour or Substance Abuse], or [Having Made Lifetime Gifts].
:::

<!-- PART 5: Funeral wishes. -->
::: funeral
1. I want my remains to be cremated *and my ashes disposed of*.

::: attestation
I have signed this Will on {{document.date}}.

::: testator
{{document.author.name.full}} ("Testator")
:::

At the request of the Testator, we were both present when the Testator signed this Will. We then signed as witnesses in the Testator's presence and in the presence of each other.

::: witnesses
::: witness
::: signatory
Signature of Witness
:::

::: name
Printed Name
:::

::: roadway
Address (Street)
:::

::: location
City, Province
:::

::: occupation
Occupation
:::
:::

::: witness
::: signatory
Signature of Witness
:::

::: name
Printed Name
:::

::: roadway
Address (Street)
:::

::: location
City, Province
:::

::: occupation
Occupation
::: <!-- occupation -->

::: <!-- witness -->
::: <!-- witnesses -->
::: <!-- attestation -->
::: <!-- funeral -->
