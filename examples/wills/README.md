# Last Will and Testament

This document provides instructions for how to create a PDF file based on the *Last Will and Testament* sample template. The template is not legal advice.

# Instructions

Create a PDF for the template as follows:

1. Browse to [https://keenwrite.com](https://keenwrite.com).
1. Download the version for your operating system.
1. Run the application (no installation needed).
1. Click **File → Open URL** (or press `Ctrl+Alt+O`).
1. Set **URL** to: `https://gitlab.com/DaveJarvis/keenwrite-themes/-/raw/main/examples/wills/last-will-and-testament.md`
1. Click **OK** (or press `Enter`) to accept.
1. Click **File → Open URL** again.
1. Set **URL** to: `https://gitlab.com/DaveJarvis/keenwrite-themes/-/raw/main/examples/wills/metadata.yaml`
1. Click **OK**; the text editor shows:
   ![Editor screenshot](images/screenshot-01.png)
1. Click **Edit → Preferences** (or press `Ctrl+Alt+s`).
1. Click **Typesetting**.
1. Set **Modes → Enable** to: `{{document.category}}`
   ![Preferences](images/screenshot-03.png)
1. Click **OK** to save and close the preferences dialog.
1. Click **File → Export As → PDF** (or press `Ctrl+p`).

Exporting for the first time will prompt you to install the typesetting software. The installation takes several minutes to complete. See the [installation video tutorial](https://www.youtube.com/watch?v=8dCui_hHK6U) to preview the installation process. After the typesetting software is installed, continue as follows:

1. Click **File → Export As → PDF**, again.
1. Set **Theme** to: `Jurisal`.
1. Click **OK** (or press `Enter`).

Open the PDF file to confirm its contents resemble the following:

![PDF file preview](images/screenshot-02.png)

Next, change the variable values and confirm the revisions as follows:

1. Double-click to expand the metadata variable hierarchy.
1. Double-click to edit values in square brackets (`[]`); values are saved automatically.
1. Revise the document to suit your circumstances (see the notes for details).
1. Export as PDF again to view the output.

Your Will is ready for review by a notary or lawyer.

# Notes

Please read and apply these notes carefully.

## Internal references

The following references may need to be updated, depending on how extensively this template has been modified: "paragraph 11", "paragraph 8(e)", and "paragraph 9(a)".

## Witnesses

Neither witness should be an individual, or spouse of an individual, named in this Will as Executor or beneficiary; witnesses must be at least 19 years old, of sound mind, and not legally blind. Witnesses are to confirm that the Will is signed by you and need not read the Will's contents.

## Sources

This template format and content is derived from the [Sample Will](https://learnlsbc.ca/sites/default/files/Wills_App_B_sample_will.pdf) chapter of *Wills Precedents—An Annotated Guide* by Peter Bogardus,
Q.C. and Mary Hamilton, published by the [Continuing Legal Education Society of British Columbia](https://www.cle.bc.ca).

This template includes ideas from [Making and Executing a Will (16:III)](https://web.archive.org/web/20221206131002/https://wiki.clicklaw.bc.ca/index.php?title=Making_and_Executing_a_Will_(16:III)) and [Wills Variation Claims (16:VI)](https://web.archive.org/web/20221126183234/https://wiki.clicklaw.bc.ca/index.php?title=Wills_Variation_Claims_(16:VI)).

This template references phrasing for disinheritance (elimination) from [How do You Disinherit Someone in BC?](https://www.stevenslaw.ca/blog/general/disinheritinbc).

## Addendum

Additional information about executing a Will may be found in [Wills Guide Witness Execution](Wills_Guide_Witness_Execution_200728_0.pdf).
