# Notice to Move Out

Date: {{document.date}}

* Tenant's Name: {{tenant.name.full}}
* Tenant's Address:  {{tenant.address.rental.roadway}}, {{tenant.address.rental.city}}, {{tenant.address.rental.region}}, {{tenant.address.rental.postal}}
* Landlord's Name: {{landlord.name.full}}
* Landlord's Address:  {{landlord.address.roadway}}, {{landlord.address.city}}, {{landlord.address.region}}, {{landlord.address.postal}}

Dear {{landlord.name.given}},

This letter serves as written notice to end my month-to-month tenancy at the address listed above. The last day of my tenancy will be **{{tenancy.vacating}}**.

Section 45(1) of the Residential Tenancy Act (RTA) states:

> 1. A tenant may end a periodic tenancy by giving the landlord notice to end the tenancy effective on a date that
>
>    (a) is not earlier than one month after the date the landlord receives the notice, and 
>
>    (b) is the day before the day the in the month, or in the other period on which the tenancy is based, that rent is payable under the tenancy agreement.   

According to section 38 of the RTA, my security deposit of ${{tenancy.deposit.security}} is to be returned within 15 days after you have received my forwarding address in writing and my tenancy has officially ended.

Please return my deposit electronically via my email address: **{{tenant.email}}**.

Alternatively, please return my deposit via postal mail to my forwarding address:

>{{tenant.name.full}}
> 
> {{tenant.address.forward.roadway}}, {{tenant.address.forward.city}}, {{tenant.address.forward.region}}, {{tenant.address.forward.postal}}

If you wish to show the rental unit to prospective tenants before the last day of my tenancy, please contact me so that we can arrange a workable schedule.

Thank you,

::: signatory
{{tenant.name.full}}
:::
