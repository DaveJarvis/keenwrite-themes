#!/usr/bin/env bash

# ---------------------------------------------------------------------------
# This script archives all files needed by ConTeXt to typeset a document.
# Included are a variety of document styles.
# ---------------------------------------------------------------------------

readonly RELEASE=$(git describe --abbrev=0 --tags)

rm -f theme-pack.zip

echo "Create theme pack"
git archive HEAD --prefix=themes/ --format=zip -o theme-pack.zip

cat tokens/release.pat | glab auth login --hostname gitlab.com --stdin

echo "Upload release"
glab release upload ${RELEASE} theme-pack.zip

echo "Add sha to Wizard.typesetter.themes.checksum"
sha256sum theme-pack.zip

rm -f theme-pack.zip

