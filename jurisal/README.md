# Overview

This theme is for typesetting legal documents.

# Annotations

Here is an example document showing the annotations recognized by the theme:

``` markdown
# Trust Agreement

::: preamble
Between:

::: settlor
{{parties.settlor.name.full}}

("{{parties.settlor.name.given}}")
:::

And:

::: trustee
{{parties.trustee.name.full}}

("{{parties.trustee.name.given}}")
:::
:::

::: recitals
1. {{parties.settlor.name.given}} intends to purchase the Property described as:
   1. PID: 000-000-000
   1. LEGAL: Strata Lot {{strata.lot}}, Suburban Block {{strata.block}}, {{address.city}} District, Strata Plan {{strata.plan}} together with an interest in the common property in proportion to the unit entitlement of the Strata Lot as shown on Form V
   1. CIVIC: {{address.number.full}} {{address.street}}, {{address.city}}, {{address.region}} {{address.postal}}

    (The "Property")

1. {{parties.trustee.name.given}} intends to hold the Property in trust for {{parties.settlor.name.given}}’s benefit.
:::

::: terms
The Parties agree as follows:

1. {{parties.settlor.name.given}} will hold $\vfrac{99}{100}$^th^ interest in the Property.
1. {{parties.trustee.name.given}} and {{parties.settlor.name.given}} will jointly hold $\vfrac{1}{100}$^th^ interest in the Property.
1. {{parties.settlor.name.given}} will hold 100% beneficial interest in the Property.
1. This agreement is to be construed in accordance with the laws of {{address.region}}, {{address.country}}.
1. The Parties may execute counterparts to this agreement.
1. The Parties may execute this agreement electronically.
:::

::: covenants
:::

::: signature
This Agreement is executed and entered into as of {{agreement.month}} ____, {{agreement.year}}, by and between:

::: signatory
{{parties.settlor.name.full}}
:::

::: signatory
{{parties.trustee.name.full}}
:::
:::
```

Note that that neither the theme nor the example are legal documents and
do not constitute legal advice.

# Fonts

This theme uses PT Serif and PT Sans. The PT Mono and Andada Pro are installed
with the typesetting container, but not used by this theme.

# Recommendation

[Matthew Butterick](https://typographyforlawyers.com/mb-fonts.html)
offers a number of high-quality fonts for legal documents, notably:

* Equity
* Concourse

These fonts are superior to their open source equivalents.

