# Overview

This theme provides typesetting instructions for song playlists. Playlists
are written as Markdown tables to be typeset using this theme. There are
many ways to create playlists, including exporting them from deejay software.

[Traktor](https://www.native-instruments.com/en/catalog/hardware/traktor) by
Native Instruments can export playlists to an NML format, which is essentially
an XML document. That XML document can be transformed to a Markdown table
using the eXtensible Stylesheet Language (XSL).

# Requirements

To create Markdown files from Traktor playlists, you'll need:

* xsltproc (install on your computer)
* stylesheet (see below)
* NML file (exported from Traktor)

# Stylesheet

The stylesheet code in this section can transform a Traktor playlist into
a Markdown table. Save the following code as `nml2md.xsl`:

``` xsl
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:strip-space elements="*" /> 
<xsl:output method="text" />

<xsl:template match="/">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="NML">
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="COLLECTION">
  <xsl:text>
| Artist | Title | Album |
|---|---|---|&#xa;</xsl:text>
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="ENTRY">
  <xsl:text>| </xsl:text><xsl:value-of select="@ARTIST" />
  <xsl:text> | </xsl:text><xsl:value-of select="@TITLE" />
  <xsl:apply-templates />
</xsl:template>

<xsl:template match="ALBUM">
  <xsl:text> | </xsl:text> <xsl:value-of select="@TITLE" />
  <xsl:text> |&#xa;</xsl:text>
</xsl:template>

<xsl:template match="*" />

</xsl:stylesheet> 
```

# Markdown

Place the exported playlist and stylesheet in the same directory (e.g.,
`/tmp` or `C:\TEMP`). Run the following command to convert the playlist
to a Markdown table:

``` bash
xsltproc nml2md.xsl playlist.nml > playlist.md
```

The following example shell script converts numerous playlists:

``` bash
#!/usr/bin/env bash

for i in $(find . -type f -name "*.nml"); do
  xsltproc nml2md.xsl "$i" > "${i%.nml}.md"
done
```

# Typeset

Once the playlist is in Markdown, use the application to convert the file
to PDF by applying this theme from the user interface or command-line as usual.

# Fonts

This theme uses [Snickles](https://fonts.keenwrite.com/download/snickles.zip).

